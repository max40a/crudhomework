package com.geekhub.userapp.web.pagination;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class PaginationUtils {

    private PaginationUtils() {
    }

    public static <T> Page<T> getPage(List<T> items, PageRequest pageRequest) {
        int size = items.size();
        int limit = pageRequest.getPerPage();
        int pageNumber = pageRequest.getPage();

        int from = (pageNumber - 1) * limit;
        int pageCount = (size % limit == 0) ? size / limit : size / limit + 1;

        return new Page<>(pageNumber, pageCount , items.stream()
                .skip(from)
                .limit(limit)
                .collect((Collectors.toCollection(ArrayList::new))));
    }
}