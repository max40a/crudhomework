<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>List of users</title>
    <link rel='stylesheet' href='webjars/bootstrap/3.1.0/css/bootstrap.min.css'>
</head>
<body>
<h1>List of users</h1>
<a href="/createUser">Create new user</a>
<hr>
<table border="1">
    <tr>
        <th>Id</th>
        <th>First Name</th>
        <th>Last Name</th>
        <th>Age</th>
        <th colspan="3">Action</th>
    </tr>
    <c:forEach items="${page.items}" var="user">
        <tr>
            <td><c:out value="${user.id}"/></td>
            <td><c:out value="${user.firstName}"/></td>
            <td><c:out value="${user.lastName}"/></td>
            <td><c:out value="${user.age}"/></td>
            <td><a href="/showUser?id=<c:out value="${user.id}"/>">show</a></td>
            <td><a href="/updateUser?id=<c:out value="${user.id}"/>">edit</a></td>
            <td><a href="/deleteUser?id=<c:out value="${user.id}"/>">delete</a></td>
        </tr>
    </c:forEach>
</table>

<button><a href="/users?page=1">first</a></button>
<button><a href="/users?page=${page.page >= page.pageCount ? page.pageCount : page.page + 1}">next</a></button>
<c:forEach begin="1" end="${page.pageCount}" var="page">
    <button><a href="/users?page=<c:out value="${page}"/>"><c:out value="${page}"/></a></button>
</c:forEach>
<button><a href="/users?page=${page.page <= 1 ? 1 : page.page - 1}">prev</a></button>
<button><a href="/users?page=${page.pageCount}">last</a></button>
</body>
</html>
