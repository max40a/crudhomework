<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <link rel='stylesheet' href='webjars/bootstrap/3.1.0/css/bootstrap.min.css'>
</head>
<body>
<h1>Create User page</h1>
<form action="/updateUser" method="post">
    <input type="hidden" name="id" value="${user.id}">
    <table border="0">
        <tr>
            <td>First Name</td>
        </tr>
        <tr>
            <td><input type="text" name="firstName" value="${user.firstName}"></td>
        </tr>
        <tr>
            <td>Last Name</td>
        </tr>
        <tr>
            <td><input type="text" name="lastName" value="${user.lastName}"></td>
        </tr>
        <tr>
            <td>Age</td>
        </tr>
        <tr>
            <td><input type="text" name="age" value="${user.age}"></td>
        </tr>
        <tr>
            <td><button><input type="submit" value="Save"></button</td>
        </tr>
    </table>
</form>
</body>
</html>
