<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Show User</title>
    <link rel='stylesheet' href='webjars/bootstrap/3.1.0/css/bootstrap.min.css'>
</head>
<body>
<table border="0">
    <tr>
        <td>First Name :</td>
        <td>${user.firstName}</td>
    </tr>
    <tr>
        <td>Last Name :</td>
        <td>${user.lastName}</td>
    </tr>
    <tr>
        <td>Age :</td>
        <td>${user.age}</td>
    </tr>
</table>
<a href="/updateUser?id=${user.id}">edit</a>&nbsp;&nbsp;<a href="/deleteUser?id=${user.id}">delete</a>
</body>
</html>
